from django.db import models
from django.contrib.auth.models import AbstractUser

class Topic(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    def __str__(self):
        return self.name

class Student(models.Model):
    firstname = models.CharField(max_length=100)
    lastname = models.CharField(max_length=100)
    email = models.EmailField()
    birthday = models.DateField()
    phone = models.CharField(max_length=20)
    # photo = models.ImageField(upload_to='cars')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.email



class Class(models.Model):
    name = models.CharField(max_length=100)
    niveau = models.CharField(max_length=100)
    status = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.niveau







# class User(AbstractUser):
#     groups = models.ForeignKey(Group, on_delete=models.CASCADE)
#     email = models.EmailField(max_length=50, unique=True)
#     my_test = models.TextField()

#     REQUIRED_FIELDS = ['groups_id', 'email']

#     class Meta:
#         verbose_name = 'user'
#         verbose_name_plural = 'users'

#     def get_full_name(self):
#         return '%s %s' % (self.first_name, self.last_name)

#     def get_short_name(self):
#         return self.first_name

#     def __str__(self):
#         return self.username