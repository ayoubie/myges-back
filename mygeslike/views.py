from django.shortcuts import render
from django.http import JsonResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response 
from .serializers import TopicSerializer, StudentSerializer, ClassSerializer
from .models import Topic, Student, Class

# Create your views here.
@api_view(['GET'])
def apiOverview(request):
    api_urls = {
        'Topics List' : '/topic-list',
        'Topic Detail View' : '/topic-detail/<str:pk>/',
        'Topic Create' : '/topic-create/',
        'Topic Update' : '/topic-update/<str:pk>/',
        'Topic Delete' : '/topic-delete/<str:pk>/',
        'List' : '/topic-list',
        'Detail View' : '/topic-detail/<str:pk>/',
        'Create' : '/topic-create/',
        'Update' : '/topic-update/<str:pk>/',
        'Delete' : '/topic-delete/<str:pk>/',
    }
    return Response(api_urls)

@api_view(['GET'])
def topicList(request):
    topics = Topic.objects.all()
    serializer = TopicSerializer(topics, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def topicDetail(request, pk):
    topics = Topic.objects.get(id=pk)
    serializer = TopicSerializer(topics, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def topicCreate(request):
    serializer = TopicSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['POST'])
def topicUpdate(request, pk):
    topics = Topic.objects.get(id=pk)
    serializer = TopicSerializer(instance=topics, data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['DELETE'])
def topicDelete(request, pk):
    topic = Topic.objects.get(id=pk)
    topic.delete()
    return Response("the topic has been deleted successfuly")


@api_view(['GET'])
def studentList(request):
    students = Student.objects.all()
    serializer = StudentSerializer(students, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def studentDetail(request, pk):
    students = Student.objects.get(id=pk)
    serializer = StudentSerializer(students, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def studentCreate(request):
    serializer = StudentSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['POST'])
def studentUpdate(request, pk):
    students = Student.objects.get(id=pk)
    serializer = StudentSerializer(instance=students, data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['DELETE'])
def studentDelete(request, pk):
    student = Student.objects.get(id=pk)
    student.delete()
    return Response("the student has been deleted successfuly")

@api_view(['GET'])
def classList(request):
    classs = Class.objects.all()
    serializer = ClassSerializer(classs, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def classDetail(request, pk):
    classs = Class.objects.get(id=pk)
    serializer = ClassSerializer(classs, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def classCreate(request):
    serializer = ClassSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['POST'])
def classUpdate(request, pk):
    classs = Class.objects.get(id=pk)
    serializer = ClassSerializer(instance=classs, data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['DELETE'])
def classDelete(request, pk):
    classs = Class.objects.get(id=pk)
    classs.delete()
    return Response("the class has been deleted successfuly")