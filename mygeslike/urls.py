from django.urls import path
from . import views


urlpatterns = [
    path('', views.apiOverview, name="api-overview"),
    path('topic-list/', views.topicList, name="topic-list"),
    path('topic-detail/<str:pk>', views.topicDetail, name="topic-detail"),
    path('topic-create/', views.topicCreate, name="topic-create"),
    path('topic-update/<str:pk>', views.topicUpdate, name="topic-update"),
    path('topic-delete/<str:pk>', views.topicDelete, name="topic-delete"),
    path('student-list/', views.studentList, name="student-list"),
    path('student-detail/<str:pk>', views.studentDetail, name="student-detail"),
    path('student-create/', views.studentCreate, name="student-create"),
    path('student-update/<str:pk>', views.studentUpdate, name="student-update"),
    path('student-delete/<str:pk>', views.studentDelete, name="student-delete"),
    path('class-list/', views.classList, name="class-list"),
    path('class-detail/<str:pk>', views.classDetail, name="class-detail"),
    path('class-create/', views.classCreate, name="class-create"),
    path('class-update/<str:pk>', views.classUpdate, name="class-update"),
    path('class-delete/<str:pk>', views.classDelete, name="class-delete"),
]
