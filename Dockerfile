FROM python:3
# set environment variables
ENV PYTHONUNBUFFERED=1
# set work directory
WORKDIR /code
COPY requirements.txt /code/
# install dependencies
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
COPY . /code/
RUN pip install djangorestframework
RUN pip install markdown
RUN pip install django-filter

# # set environment variables
# ENV PYTHONDONTWRITEBYTECODE 1
# ENV PYTHONUNBUFFERED 1

# # install dependencies
# RUN pip install --upgrade pip
# COPY ./requirements.txt .
# RUN pip install -r requirements.txt

# # copy project
# COPY . .


